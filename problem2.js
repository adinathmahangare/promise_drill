const fs = require('fs').promises;

function processFile(lipsumPath, fileNamesPath) {
    let newFileNames = [];

    fs.readFile('lipsum_2.txt', 'utf-8')
        .then(data => {
            const uppercaseData = data.toUpperCase();
            return fs.writeFile('lipsum_uppercase.txt', uppercaseData);
        })
        .then(() => {
            newFileNames.push('lipsum_uppercase.txt');
            return fs.readFile('lipsum_uppercase.txt', 'utf-8');
        })
        .then((uppercaseData) => {
            const lowerCaseData = uppercaseData.toLowerCase();
            const sentences = lowerCaseData.split(/[.!?]+/);
            return fs.writeFile('lipsum_sentences.txt', sentences.join('\n'));
        })
        .then(() => {
            newFileNames.push('lipsum_sentences.txt');
            return fs.appendFile('filenames.txt', newFileNames.join('\n') + '\n');
        })
        .then(() => {
            return fs.readFile('filenames.txt', 'utf8');
        })
        .then(fileNames => {
            const filesToDelete = fileNames.trim().split('\n');
            const deletePromises = filesToDelete.map(fileName => fs.unlink(fileName.trim()));
            return Promise.all(deletePromises);
        })
        .then(() => {
            console.log('All files processed and deleted successfully.');
        })
        .catch(error => {
            console.error('An error occurred:', error);
        });
}

module.exports = processFile;
