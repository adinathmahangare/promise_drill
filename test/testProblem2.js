const fs = require('fs').promises;
const processFile = require('./../problem2');


fs.writeFile('filenames.txt', '')
    .then(() => {
        console.log('filenames.txt created successfully.');
        processFile(); 
    })
    .catch(error => {
        console.error('An error occurred while creating filenames.txt:', error);
    });