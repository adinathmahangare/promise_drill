const fs = require('fs').promises;

function createAndDeleteRandomJsonFiles(directoryName, numOfFiles) {
    fs.mkdir(directoryName, { recursive: true })
        .then(() => {
            console.log('folder created');

            const createPromises = [];
            for (let index = 0; index < numOfFiles; index++) {
                const filePath = `${directoryName}/file${index}.json`;
                createPromises.push(fs.writeFile(filePath, JSON.stringify(`Hi ${index}`)));
            }
            return Promise.all(createPromises);
        })
        .then(() => {
            console.log('All files Created');
            const deletePromises = [];
            for (let index = 0; index < numOfFiles; index++) {
                const filePath = `${directoryName}/file${index}.json`;
                deletePromises.push(fs.unlink(filePath));
            }

            return Promise.all(deletePromises) 
            .then(() => console.log('All files deleted'))
            .catch(error => console.log(`Error deleting files: ${error.message}`));
        })
        .catch(error => console.error(error));
}

module.exports = {
    createAndDeleteRandomJsonFiles
}

